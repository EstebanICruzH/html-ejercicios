//ejer1
//¿Qué regresa la siguiente función?
function f(x, y=2, z=7){
 return x+y+z;
}
console.log(f(5,undefined))
//resultado es 14, ya que el valor x en el cosole lo define como 5 y suma las otras variables

//ejer2
//¿Cuál es el resultado en la variable result
var animal = 'kitty';

var result = ( animal === 'kitty') ? 'cute' : 'still nice';
//result es igaul a  'cute' ya que la comparación fue verdadera, al ser variables del mismo tipo
//ejer 3
//Genera una función e imprime en consola el resultado
        //La función, recibe como parametro una variable con string = kitty
        function resultKitty(animal = 'kitty'){
            //hace una comparación de la variable
            if(animal === 'kitty'){
                //si se cumple la funcion, la variable result recibe un string con la palabra 'cute'
                var result = 'cute';
            } else {
                //si no se cumple, result recibe el string still 'nice'
                var result = 'still nice';
            }
            //retorna la variable result
            return result;
        }
        

//ejer 4
//Genera una función e imprime en consola el resultado
    var a=0;
    var str = 'not a';
    var b = '';

    function ejer4 (a){
        if(  a === 0){ 
        a = 1
        str += ' test'
        b = str 
        }else {
            a= 2
        }
        return b;
    }
          //se compara la variable a, si es igual a 0 le agrega 'test a la variable str'
        //si no, a cambia a 2

//ejer 5
//Genera una función e imprime en consola el resultado
var a = 1;
function ejer5(a){
if(a === 1){
    alert ('hey, eso es 1!')
}else { 
 a= 0;
}

}
//Recibe un parametro, si el parametro es 1 manda una alert, si no, no manda nada

 

//ejer 6
//Genera una función e imprime en consola el resultado
function ejer6(a){ 
if(a === 1){
    alert('hey, eso es 1!') 
}else {
    alert('weird, what could it be?')
}

}
 //Recibe un parametro, lo compara, si es uno manda alerta que es uno
//, si no manda alerta con otro mensaje

//ejer 7
//Genera una funcion e imprime en consola el resultado
function ejer7(animal){
    for(var i=0; i<5; ++i){
        if(animal==='kitty'){
            break;
        }else{
            console.log(i);
        }
    }
    return animal;
}
//La función toma el parametro de animal, si el parametro es 'kitty', se termina el codigo
//en caso de que nos sea 'kitty', recorre el ciclo for

//ejer 8
//Genera una función e imprime en consola el resutlado
function ejer8(val){
    switch(val){
        case 1:
        console.log('Yo siempre corro');
        break;
        case 2:
            console.log('Yo nunca corro')
            break;
    }
    return val;
}
//La función recibe un parametro y dependiendo del valor obtenga,
//será la accion del case que realizara

//ejer 9
var animal = 'Lion';
function ejer9(animal){
switch(animal){
    case 'Dog':
        console.log('I will not run since animal !== "Dog"')
    break;
    case 'Cat':
        console.log('I will not run sice animal !== "Cat"')
        break;
        default:
            console.log('I will rin since animal does not mach any other case')
  }

}
//La función recibe un parametro y dependiendo del valor obtenido,
//será la accion que el case realizara y si obtiene otro valor tambien mostrara un resultado


//ejer10

function john(){
    return 'John';
}

function jacob(){
    return 'Jacob';
}

function ejer10(name){
    switch (name){
        case john():
            console.log('I will run if name === "John"');
            break;
        case 'Ja' + 'ne':
            console.log('I will run if name === "Jane"')
            break;
        case john() + ' ' + jacob() + ' Jingleheimer Schmidt':
            console.log('His name is equal to name too!')
            break;
    }
}
//La función recibe un parametro de nombre y dependiendo del nombre hace la funcion del switch
//deberá de invocar a la función dentro de la misma y de ahi, tomar el parametro para el switch

//ejer11

var x = 'c'

function ejer11(x){
    switch(x){ 
    case 'a':
        case 'b':
            case 'c':
                console.log('Either a, b, or c was selected. ');
                break;
                case 'd':
                    console.log('Only d was selected.');
                    break;
                    default:
                        console.log('No case was matched.');
                        break;

    }
    return x;
}
//La función recibe un paramentro y si el parametro es a, b o c, mandara el mismo mensaje
//si recibe otro, mandara un mensaje diferente


//ejer12

var x = 5 + 7;// el resultado es 12
var x = 5 + "7";//el resultado es 57 las comillas en el 7 y el signo + hace que se junten los dos valores
var x = "5" + 7;//el resultado es 57, de igual forma las comilla en el 5 y el signo + hace que se junten los dos valores
var x = 5 - 7;//el resultado es -2
var x = 5 - "7";//el resultado es -2, supongo que no se unen por que signo -
var x = "5" - 7;//el resultado es -2, igual que en el aterior, no se unen por que tiene el signo -
var x = 5 - "x";//el resultado es NaN, será por que la x no tiene ningun valor.

//ejer13

var a= 'hello' || '';//el resultado es ello
var b= '' || [];//el resultado es []
var c= '' || undefined;// el resultado es undefined
var d= 1 || 5;//el resultado es 1
var e= 0 || {};//el resultado es {}
var f= 0 || '' || 5;//el resultado es 5
var g= '' || 'yay' || 'boo';//el resultado es 'yay' por que se encuentra antes de 'boo'

//ejer14

var a= 'hello' && '';// el resultado es "".
var b= '' && [];//el resultado es el mismo "".
var c= '' && undefined;//el resultado es ""
var d= 1 && 5;//el resultado es 5
var e= 0 && {};//el resultado es 0
var f= 'hi' && [] && 'done';//el resultado es done
var g= 'bye' && undefined && 'adios';//el resultado es undefined
  
//ejer15
 //Teniendo la funcion foo, cual es el resultado de cada sentencia
  var foo = function(val){
      return val || 'default';

  }
  console.log (foo ('burger'));//el resultado es burger
  console.log (foo (100));//el resultado es 100
  console.log (foo ([]));//el resultado es []
  console.log (foo (0));//el resultado es default
  console.log (foo (undefined));//el resultado es default
//Teniendo la funcion foo
//en todos retorna el valor obtenido por el usuario y les da prioridad

 
  //ejer16
//imprime el valor dado por el ususario
//en todas saldra error asi que les tendremos que asignar un valor
var isLegal = age >= 18; //se cumple la sentencia si la variable edad, es igual o mayor a 18
var tall = height >= 5.11; //se cumple la sentencia si la variable height es igual o mayor a 5.11
var suitable = isLegal && tall;// se cumple la sentencia si la sentencia isLegal y tall se cumplieron
var isRoyalty = status === 'royalty';// se cumple la sentencia si la variable status es royalty
var specialCase = isRoyalti && hasInvitation;// se cumple la sentencia si las sentencias isroyalty y hasinvitation se cumplen
var canEnterOurBar = suitable || specialCase;// se cumple la sentencia si alguna de las dos se cumplen

  //ejer17

  function ejer17(){
      for (var i=0; i<3; i++){
          if(i=== 1){
              continue;
          }
          console.log(i);
      }
      return ejer17();
  }
//Recorre el ciclo for y al llegar a 1 la variable i
//continua al siguiente y no se muestra


  //ejer18
  var i= 0;

  function ejer18(){
      while(i < 3){
          if (i === 1){
              i = 2;
              continue;
          }
          console.log(i);
          i++;
      }
  }
   //Parecido al anterior, se hace un recorrido con el while
   // pero al llegar a 1, se pasa al siguiente y i se covierte en 2

  //ejer19
  function ejer19(){

      for(var i = 0; i< 5; i++){
          nextLoop2Iteration;
          for(var j= 0; j < 5; j++){
              if(i==j) break
                 nextLoop2Iteration;
                  console.log(i, j);
              }
          }

      }
  


//ejer20

function foo(){
    var a = 'hello';

        function bar(){
            var b = 'word';
            console.log(a);
            console.log(b);
        }
        console.log(a);
        console.log(b);
}
    console.log(a);
    console.log(b);


  //ejer21
  
  function foo(){
      const a= true;
  
  function bar(){
      const a = false;
      console.log(a);
  }

  const a = false;
  a = false;
  console.log(a);
}
  


//ejer22
function ejer22(){ 
var namedSum = function sum (a,b){
    return a + b;
}
var anonSum = function(a, b){
    return a + b;
}
}

// Nos muestra las formas de declarar una función que hace la suma de dos variables.


//ejer23

function errores(){
    try{
        funcion_que_no_existe();

    } catch(ex){
        console.log("Error detectado: " + ex.description);
    }
    console.log('continua con la función aun despues del error');
}
//las sentencias try-catch nos permite continuar con la ejecución de un código, 
//aunque haya encontrado algún error.


